importScripts('/_nuxt/workbox.3de3418b.js')

const workboxSW = new self.WorkboxSW({
  "cacheId": "cloud",
  "clientsClaim": true,
  "directoryIndex": "/"
})

workboxSW.precache([
  {
    "url": "/_nuxt/app.4ab626b661dd4834d70c.js",
    "revision": "507f4e2d410ac07fb38440898beeb897"
  },
  {
    "url": "/_nuxt/app.ba00db15d6ab3339eeecb97c98891177.css",
    "revision": "ba00db15d6ab3339eeecb97c98891177"
  },
  {
    "url": "/_nuxt/layouts/default.4cf5668be174dfb06720.js",
    "revision": "faf00b19501124bffb56351ad49f8cb7"
  },
  {
    "url": "/_nuxt/manifest.f5ed28ec406292173195.js",
    "revision": "1dfaaa8ed46a56c7e965552aace6d140"
  },
  {
    "url": "/_nuxt/pages/admin/index.846ce8883ed70f389e51.js",
    "revision": "ab82c27abb4f54b4512f566195646982"
  },
  {
    "url": "/_nuxt/pages/index.db2522a1c9c1dd628c81.js",
    "revision": "c4d5362f20d94cacd271616630916efe"
  },
  {
    "url": "/_nuxt/pages/users/_username/index.2cb06ad65a0b60e3f302.js",
    "revision": "d6bb2d7f30e49af050b09bd02e1187a0"
  },
  {
    "url": "/_nuxt/pages/users/auth/index.621aab3101b5c1b3280b.js",
    "revision": "0db767f8cda2e3f64a05c7a527286e63"
  },
  {
    "url": "/_nuxt/pages/users/auth/sign-in.4dec12b8b6c2cb10619d.js",
    "revision": "9ce3c83340bf5aa3d4d94f3fa448b60f"
  },
  {
    "url": "/_nuxt/pages/users/auth/sign-out.596e0be0385cc073de0e.js",
    "revision": "2883f2bf56decf11d2b1754bc3298fc5"
  },
  {
    "url": "/_nuxt/pages/users/auth/sign-up.07e710f76f8046536bf4.js",
    "revision": "82222bbd311f90a16a392ebd874a9280"
  },
  {
    "url": "/_nuxt/pages/users/index.6f9b911431a416b587aa.js",
    "revision": "b225ab6f18f52697d5cb91dd170270b0"
  },
  {
    "url": "/_nuxt/vendor.eac1c077f4c747d804f6.js",
    "revision": "8865e0ba903248ee98d18b0ba342ecbd"
  }
])


workboxSW.router.registerRoute(new RegExp('/_nuxt/.*'), workboxSW.strategies.cacheFirst({}), 'GET')

workboxSW.router.registerRoute(new RegExp('/.*'), workboxSW.strategies.networkFirst({}), 'GET')

